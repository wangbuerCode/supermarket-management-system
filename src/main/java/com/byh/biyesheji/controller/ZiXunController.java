package com.byh.biyesheji.controller;

import com.byh.biyesheji.pojo.ZiXun;
import com.byh.biyesheji.pojo.ZiXun_model;
import com.byh.biyesheji.service.ZiXunService;
import com.byh.biyesheji.util.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.sun.org.apache.xpath.internal.operations.Mod;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.text.SimpleDateFormat;
import java.util.*;

@Controller
@RequestMapping("/zixun")
public class ZiXunController {

    @Autowired
    private ZiXunService ziXunService;

    @RequestMapping("/list")
    public String list(Page page, Model model){
        PageHelper.offsetPage(page.getStart(),page.getCount());//分页查询
        List<ZiXun> list = ziXunService.list1();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        List<ZiXun_model> list1 = new ArrayList<ZiXun_model>();
        for(ZiXun z : list){
            ZiXun_model ziXun_model = new ZiXun_model();
            ziXun_model.setContent(z.getContent());
            ziXun_model.setCstid(z.getCstid());
            ziXun_model.setCustomer(z.getCustomer());
            ziXun_model.setHuifu(z.getHuifu());
            ziXun_model.setId(z.getId());
            ziXun_model.setStatus(z.getStatus());
            ziXun_model.setFabudate(sdf.format(z.getFabudate()));
            list1.add(ziXun_model);
        }
        int total = (int) new PageInfo<>(list).getTotal();//总条数
        page.setTotal(total);

        model.addAttribute("list",list1);
        model.addAttribute("totals",total);

        return "cstpage/zixun-list";
    }

    /**
     * 审核
     * @param zid
     * @return
     */
    @RequestMapping("/zixunshenhe")
    @ResponseBody
    public String zixunshenhe(int zid){
        ziXunService.shenhe(zid);
        return "success";
    }

    /**
     * 回复留言
     * @param id
     * @return
     */
    @RequestMapping("/huifu")
    public String huifu(@RequestParam(value = "id") int id,Model model){
        model.addAttribute("id",id);
        return "cstpage/zixun-edit";
    }

    /**
     * 管理员回复
     * @param
     * @return
     */
    @RequestMapping("/upthuifu")
    public String upthuifu(ZiXun ziXun,Model model){
        ziXun.setStatus(1);
        ziXunService.uphuifu(ziXun);
        return "redirect:list";
    }

    /**
     * 删除留言
     */
    @RequestMapping("/del")
    public String del(Model model,int id){
        ziXunService.del(id);
        return "redirect:list";
    }
}
