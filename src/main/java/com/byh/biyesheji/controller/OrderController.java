package com.byh.biyesheji.controller;


import com.byh.biyesheji.pojo.Order;
import com.byh.biyesheji.pojo.Order_2;
import com.byh.biyesheji.service.AddressService;
import com.byh.biyesheji.service.OrderItemService;
import com.byh.biyesheji.service.OrderService;
import com.byh.biyesheji.util.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import jdk.nashorn.internal.ir.IdentNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * 订单模块controller
 */
@Controller
@RequestMapping("/order")
public class OrderController {

    @Autowired
    OrderService orderService;
    @Autowired
    OrderItemService orderItemService;
    @Autowired
    AddressService addressService;

    /**
     * 所有订单
     * @param model
     * @param page
     * @return
     */
    @RequestMapping("/list")
    public String list(Model model, Page page){
        PageHelper.offsetPage(page.getStart(),page.getCount());

        List<Order> os= orderService.list();

        int total = (int) new PageInfo<>(os).getTotal();
        page.setTotal(total);
        //为订单添加订单项数据
        orderItemService.fill(os);

        List<Order_2> order2s = new ArrayList<Order_2>();
        for(Order o:os){
            Order_2 order_2 = new Order_2();
            order_2.setCode(o.getCode());
            order_2.setCstid(o.getCstid());
            order_2.setCustomer(o.getCustomer());
            order_2.setId(o.getId());
            order_2.setOrderItems(o.getOrderItems());
            order_2.setStatus(o.getStatus());
            order_2.setTotal(o.getTotal());
            order_2.setTotalNumber(o.getTotalNumber());
            order_2.setGen_address(addressService.get(o.getAddress_id()).getGen_address());
            order_2.setGen_name(addressService.get(o.getAddress_id()).getGen_name());
            order_2.setTelphone(addressService.get(o.getAddress_id()).getTelphone());
            order2s.add(order_2);
        }

        model.addAttribute("os", order2s);
        model.addAttribute("page", page);
        model.addAttribute("totals", total);

        return "ordermodule/order-list";
    }

    /**
     * 所有订单
     * @param model
     * @param page
     * @return
     */
    @RequestMapping("/creditlist")
    public String creditlist(Model model, Page page){
        PageHelper.offsetPage(page.getStart(),page.getCount());

        List<Order> os= orderService.creditlist();

        int total = (int) new PageInfo<>(os).getTotal();
        page.setTotal(total);
        //为订单添加订单项数据
        orderItemService.fill(os);

        List<Order_2> order2s = new ArrayList<Order_2>();
        for(Order o:os){
            Order_2 order_2 = new Order_2();
            order_2.setCode(o.getCode());
            order_2.setCstid(o.getCstid());
            order_2.setCustomer(o.getCustomer());
            order_2.setId(o.getId());
            order_2.setOrderItems(o.getOrderItems());
            order_2.setStatus(o.getStatus());
            order_2.setTotal(o.getTotal());
            order_2.setTotalNumber(o.getTotalNumber());
            order_2.setGen_address(addressService.get(o.getAddress_id()).getGen_address());
            order_2.setGen_name(addressService.get(o.getAddress_id()).getGen_name());
            order_2.setTelphone(addressService.get(o.getAddress_id()).getTelphone());
            order2s.add(order_2);
        }

        model.addAttribute("os", order2s);
        model.addAttribute("page", page);
        model.addAttribute("totals", total);

        return "billmodule/credit-list";
    }
    /**
     * 所有订单
     * @param model
     * @param page
     * @return
     */
    @RequestMapping("/paylist")
    public String paylist(Model model, Page page){
        PageHelper.offsetPage(page.getStart(),page.getCount());

        List<Order> os= orderService.paylist();

        int total = (int) new PageInfo<>(os).getTotal();
        page.setTotal(total);
        //为订单添加订单项数据
        orderItemService.fill(os);

        List<Order_2> order2s = new ArrayList<Order_2>();
        for(Order o:os){
            Order_2 order_2 = new Order_2();
            order_2.setCode(o.getCode());
            order_2.setCstid(o.getCstid());
            order_2.setCustomer(o.getCustomer());
            order_2.setId(o.getId());
            order_2.setOrderItems(o.getOrderItems());
            order_2.setStatus(o.getStatus());
            order_2.setTotal(o.getTotal());
            order_2.setTotalNumber(o.getTotalNumber());
            order_2.setGen_address(addressService.get(o.getAddress_id()).getGen_address());
            order_2.setGen_name(addressService.get(o.getAddress_id()).getGen_name());
            order_2.setTelphone(addressService.get(o.getAddress_id()).getTelphone());
            order2s.add(order_2);
        }

        model.addAttribute("os", order2s);
        model.addAttribute("page", page);
        model.addAttribute("totals", total);

        return "billmodule/bill-list";
    }
    /**
     * 订单发货
     * @param o
     * @return
     */
    @RequestMapping("/orderDelivery")
    public String delivery(Order o){
        o.setStatus(2);
        orderService.update(o);
        return "redirect:list";
    }
    @RequestMapping("/orderPay")
    public String orderPay(Order o){
        o.setStatus(2);
        orderService.update(o);
        return "redirect:list";
    }
    @RequestMapping("/orderCredit")
    public String orderCredit(Order o){
        o.setStatus(3);
        orderService.update(o);
        return "redirect:list";
    }

    /**
     * 管理员同意取消订单
     * @param o
     * @return
     */
    @RequestMapping("/okcancel")
    @ResponseBody
    public String okcancel(Order o){
        Order order = orderService.get(o.getId());
        order.setStatus(5);//管理员同意取消，订单关闭
        orderService.update(order);
        return "redirect:list";
    }
    @RequestMapping("nocancel")
    @ResponseBody
    public String nocancel(Order o){
        Order order = orderService.get(o.getId());
        order.setStatus(6);//管理员不同意取消，继续发货
        orderService.update(order);
        return "redirect:list";
    }

    /**
     * 查看当前订单的订单项
     * @param oid
     * @param model
     * @return
     */
    @RequestMapping("/seeOrderItem")
    public String seeOrderItem(int oid,Model model){
        Order o = orderService.get(oid);
        orderItemService.fill(o);
        model.addAttribute("orderItems",o.getOrderItems());
        model.addAttribute("total",o.getOrderItems().size());
        model.addAttribute("totalPrice",o.getTotal());
        return "ordermodule/orderItem-list";
    }

}
