package com.byh.biyesheji.controller;

import com.byh.biyesheji.pojo.Product;
import com.byh.biyesheji.pojo.Provider;
import com.byh.biyesheji.pojo.Purchase;
import com.byh.biyesheji.pojo.Waste;
import com.byh.biyesheji.service.ProductService;
import com.byh.biyesheji.service.ProviderService;
import com.byh.biyesheji.service.PurchaseService;
import com.byh.biyesheji.service.WasteService;
import com.byh.biyesheji.util.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/waste")
public class WasteController {

    @Autowired
    private WasteService wasteService;
    @Autowired
    private PurchaseService purchaseService;

    @RequestMapping("/wasteAddUI")
    public String addUI(Model model){
        List<Purchase> purchaseList = purchaseService.list();
        model.addAttribute("purchaselist",purchaseList);
        return "provider/waste-add";
    }
    @RequestMapping("/editWaste")
    public String editUI(@RequestParam(value = "id")int id,Model model){
        //获得要修的信息
        List<Purchase> purchaseList = purchaseService.list();
        Waste waste = wasteService.get(id);
        Purchase purchase = purchaseService.get(waste.getProid());
        model.addAttribute("crrentPurchase",purchase);
        model.addAttribute("waste",waste);
        model.addAttribute("purchaseList",purchaseList);
        return "provider/waste-edit";
    }
    @RequestMapping("/list")
    public String list(Page page, Model model){
        PageHelper.offsetPage(page.getStart(),page.getCount());//分页查询
        List<Waste> list = wasteService.list();
        int total = (int) new PageInfo<>(list).getTotal();//总条数
        page.setTotal(total);
        model.addAttribute("list",list);
        model.addAttribute("totals",total);
        return "provider/waste-list";
    }
    @RequestMapping("/getByProid")
    @ResponseBody
    public Map<String,Object> getByProid(Integer id){
        Purchase purchase = purchaseService.get(id);
        Map<String,Object> map = new HashMap<>();
        map.put("price",purchase.getPrice());
        return map;
    }
    /**
     * 增加
     */
    @RequestMapping("/addWaste")
    public String addWaste(Waste waste){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        waste.setAddtime(sdf.format(new Date()));
        Purchase purchase = purchaseService.get(waste.getProid());
        waste.setName(purchase.getName());
        wasteService.save(waste);
        return "redirect:list";
    }
    /**
     * 修改
     */
    @RequestMapping("/updateWaste")
    public String updateWaste(Waste waste){
        wasteService.update(waste);
        return "redirect:list";
    }
    /**
     * 删除
     */
    @RequestMapping("/del")
    public String del(Model model,int id){
        wasteService.del(id);
        return "redirect:list";
    }
}
