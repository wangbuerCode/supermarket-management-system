package com.byh.biyesheji.controller;

import com.byh.biyesheji.pojo.Category;
import com.byh.biyesheji.pojo.Provider;
import com.byh.biyesheji.pojo.Purchase;
import com.byh.biyesheji.service.ProviderService;
import com.byh.biyesheji.service.PurchaseService;
import com.byh.biyesheji.util.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/purchase")
public class PurchaseController {

    @Autowired
    private PurchaseService purchaseService;
    @Autowired
    private ProviderService providerService;

    @RequestMapping("/purchaseAddUI")
    public String addUI(Model model){
        List<Provider> providers = providerService.list();
        model.addAttribute("providerlist",providers);
        return "provider/purchase-add";
    }
    @RequestMapping("/editPurchase")
    public String editUI(@RequestParam(value = "id")int id,Model model){
        //获得要修的信息
        List<Provider> providers = providerService.list();
        Purchase purchase = purchaseService.get(id);
        Provider provider = providerService.get(purchase.getProviderid());
        model.addAttribute("crrentProvider",provider);
        model.addAttribute("purchase",purchase);
        model.addAttribute("providerlist",providers);
        return "provider/purchase-edit";
    }
    @RequestMapping("/list")
    public String list(Page page, Model model){
        PageHelper.offsetPage(page.getStart(),page.getCount());//分页查询
        List<Purchase> list = purchaseService.list();
        int total = (int) new PageInfo<>(list).getTotal();//总条数
        page.setTotal(total);
        model.addAttribute("list",list);
        model.addAttribute("totals",total);
        return "provider/purchase-list";
    }
    /**
     * 增加
     */
    @RequestMapping("/addPurchase")
    public String addProvider(Purchase purchase){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        purchase.setAddtime(sdf.format(new Date()));
        Provider provider = providerService.get(purchase.getProviderid());
        purchase.setProvidername(provider.getCompany());
        purchaseService.save(purchase);
        return "redirect:list";
    }
    /**
     * 修改
     */
    @RequestMapping("/updatePurchase")
    public String updateProvider(Purchase purchase){
        purchaseService.update(purchase);
        return "redirect:list";
    }
    /**
     * 删除
     */
    @RequestMapping("/del")
    public String del(Model model,int id){
        purchaseService.del(id);
        return "redirect:list";
    }
}
