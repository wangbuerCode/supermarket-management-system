package com.byh.biyesheji.controller;

import com.byh.biyesheji.pojo.Order;
import com.byh.biyesheji.pojo.Order_3;
import com.byh.biyesheji.pojo.Review;
import com.byh.biyesheji.pojo.Review_model;
import com.byh.biyesheji.service.CustomerService;
import com.byh.biyesheji.service.OrderService;
import com.byh.biyesheji.service.ProductService;
import com.byh.biyesheji.service.ReviewService;
import com.byh.biyesheji.util.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * 评论模块controller
 */
@Controller
@RequestMapping("/review")
public class ReviewController {

    @Autowired
    private ReviewService reviewService;
    @Autowired
    private ProductService productService;
    @Autowired
    private OrderService orderService;
    @Autowired
    private CustomerService customerService;

    @RequestMapping("/relist")
    public String relist(Model model, Page page){
        PageHelper.offsetPage(page.getStart(),page.getCount());//分页查询
        List<Review> list= reviewService.list();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        List<Review_model> list1 = new ArrayList<Review_model>();
        for(Review rw : list){
            Review_model review = new Review_model();
            review.setCustomer(rw.getCustomer());
            review.setProduct(rw.getProduct());
            review.setContent(rw.getContent());
            review.setCreatetime(sdf.format(rw.getCreatetime()));
            review.setCstid(rw.getCstid());
            review.setPid(rw.getPid());
            review.setId(rw.getId());;
            list1.add(review);
        }
        int total = (int) new PageInfo<>(list).getTotal();//总条数
        page.setTotal(total);

        model.addAttribute("totals",list.size());
        model.addAttribute("list",list);
        return "review/review";
    }
    @RequestMapping("/list")
    public String list(Model model, Page page) throws Exception{
        PageHelper.offsetPage(page.getStart(),page.getCount());//分页查询
//        List<Review> list= reviewService.list();
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
//        List<Review_model> list1 = new ArrayList<Review_model>();
//        for(Review rw : list){
//            Review_model review = new Review_model();
//            review.setCustomer(rw.getCustomer());
//            review.setProduct(rw.getProduct());
//            review.setContent(rw.getContent());
//            review.setCreatetime(sdf.format(rw.getCreatetime()));
//            review.setCstid(rw.getCstid());
//            review.setPid(rw.getPid());
//            review.setId(rw.getId());;
//            list1.add(review);
//        }
        List<Order> list1 = orderService.list();
        List<Order_3> list = new ArrayList<Order_3>();
        for (Order o : list1){
            Order_3 order_3 = new Order_3();
            order_3.setId(o.getId());
            order_3.setCode(o.getCode());
            order_3.setCustomer_name(customerService.get(o.getCstid()).getName());
            order_3.setPingjia(o.getPingjia());
            order_3.setPingjia_guanli(o.getPingjia_guanli());
            list.add(order_3);
        }
        int total = (int) new PageInfo<>(list).getTotal();//总条数
        page.setTotal(total);

        model.addAttribute("totals",list.size());
        model.addAttribute("list",list);
        return "pinglunpage/pinglun";
    }

    @RequestMapping("/del")
    public String del(int id){
        reviewService.del(id);
        return "redirect:relist";
    }
//    /**
//     * 管理员回复
//     */
    @RequestMapping("/upthuifu")
    public String huifu(Order order,Model model){
        orderService.update(order);
        return "redirect:list";
    }

    /**
     * 管理员回复评价
     * @param review
     * @param model
     * @return
     */
    @RequestMapping("/rehuifucon")
    public String rehuifucon(Review review,Model model){
        Review review1 = reviewService.get(review.getId());
        review1.setHuifu(review.getHuifu());
        reviewService.update(review1);
        return "redirect:relist";
    }


    /**
     * 管理员回复界面
     */
    @RequestMapping("/huifu")
    public String huifu(@RequestParam(value = "id") Integer id, Model model){
        model.addAttribute("id",id);
        return "pinglunpage/pinglun-edit";
    }

    /**
     * 管理员回复商品评价
     * @param id
     * @param model
     * @return
     */
    @RequestMapping("/rehuifu")
    public String rehuifu(@RequestParam(value = "id") Integer id, Model model){
        model.addAttribute("id",id);
        return "review/review-edit";
    }
}
