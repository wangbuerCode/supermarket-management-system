package com.byh.biyesheji.controller;

import com.byh.biyesheji.pojo.*;
import com.byh.biyesheji.service.ProviderService;
import com.byh.biyesheji.service.ZiXunService;
import com.byh.biyesheji.util.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/provider")
public class ProviderController {

    @Autowired
    private ProviderService providerService;

    @RequestMapping("/providerAddUI")
    public String addUI(Model model){
        return "provider/provider-add";
    }
    @RequestMapping("/editProvider")
    public String editUI(@RequestParam(value = "id")int id,Model model){
        //获得要修改供应商的信息
        Provider provider = providerService.get(id);
        model.addAttribute("provider",provider);
        return "provider/provider-edit";
    }
    @RequestMapping("/list")
    public String list(Page page, Model model){
        PageHelper.offsetPage(page.getStart(),page.getCount());//分页查询
        List<Provider> list = providerService.list();
        int total = (int) new PageInfo<>(list).getTotal();//总条数
        page.setTotal(total);
        model.addAttribute("list",list);
        model.addAttribute("totals",total);
        return "provider/provider-list";
    }
    /**
     * 增加供应商
     */
    @RequestMapping("/addProvider")
    public String addProvider(Provider provider){
        providerService.save(provider);
        return "redirect:list";
    }
    /**
     * 修改供应商
     */
    @RequestMapping("/updateProvider")
    public String updateProvider(Provider provider){
        providerService.update(provider);
        return "redirect:list";
    }
    /**
     * 删除留言
     */
    @RequestMapping("/del")
    public String del(Model model,int id){
        providerService.del(id);
        return "redirect:list";
    }
}
