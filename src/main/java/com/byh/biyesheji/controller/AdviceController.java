package com.byh.biyesheji.controller;

import com.byh.biyesheji.pojo.Advice;
import com.byh.biyesheji.pojo.ZiXun;
import com.byh.biyesheji.pojo.ZiXun_model;
import com.byh.biyesheji.service.AdviceService;
import com.byh.biyesheji.service.ZiXunService;
import com.byh.biyesheji.util.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/advice")
public class AdviceController {

    @Autowired
    private AdviceService adviceService;

    @RequestMapping("/list")
    public String list(Page page, Model model){
        PageHelper.offsetPage(page.getStart(),page.getCount());//分页查询
        List<Advice> list = adviceService.list();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        int total = (int) new PageInfo<>(list).getTotal();//总条数
        page.setTotal(total);
        model.addAttribute("list",list);
        model.addAttribute("totals",total);
        return "cstpage/advice-list";
    }

    /**
     * 删除
     */
    @RequestMapping("/del")
    public String del(Model model,int id){
        adviceService.del(id);
        return "redirect:list";
    }
    /**
     * 添加
     */
    @RequestMapping("/addAdvice")
    public String addAdvice(String content){
        Advice advice = new Advice();
        advice.setContent(content);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        advice.setAddtime(sdf.format(new Date()));
        adviceService.save(advice);
        return "redirect:list";
    }
}
