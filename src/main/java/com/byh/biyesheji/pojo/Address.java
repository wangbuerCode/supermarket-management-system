package com.byh.biyesheji.pojo;

import lombok.ToString;

/**
 * @ClassName Address
 * @Description TODO
 * @Author 陈志星
 * @Date 2020/2/24 10:33
 */
@ToString
public class Address {

    private Integer id;
    private Integer customer_id;
    private String telphone;
    private String gen_address;
    private String gen_name;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(Integer customer_id) {
        this.customer_id = customer_id;
    }

    public String getTelphone() {
        return telphone;
    }

    public void setTelphone(String telphone) {
        this.telphone = telphone;
    }

    public String getGen_address() {
        return gen_address;
    }

    public void setGen_address(String gen_address) {
        this.gen_address = gen_address;
    }

    public String getGen_name() {
        return gen_name;
    }

    public void setGen_name(String gen_name) {
        this.gen_name = gen_name;
    }
}
