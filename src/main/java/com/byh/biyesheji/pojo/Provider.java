package com.byh.biyesheji.pojo;

/**
 * @ClassName Provider
 * @Description TODO
 * @Author 陈志星
 * @Date 2020/4/2 13:42
 */
public class Provider {

    private Integer id;
    private String company;
    private String name;
    private String tel;
    private String scope;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }
}
