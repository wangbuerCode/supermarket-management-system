package com.byh.biyesheji.pojo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
public class Order_3 {

    private Integer id;

    private String code;//订单编号

    private String address;

    private Integer address_id;

    private Integer status;

    private Integer cstid;

    private List<OrderItem> orderItems;

    private Customer customer;

    private String customer_name;

    private float total;  //总价

    private int totalNumber; //总数量

    private String pingjia;

    private String pingjia_guanli;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getCstid() {
        return cstid;
    }

    public void setCstid(Integer cstid) {
        this.cstid = cstid;
    }

    public List<OrderItem> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(List<OrderItem> orderItems) {
        this.orderItems = orderItems;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public int getTotalNumber() {
        return totalNumber;
    }

    public Integer getAddress_id() {
        return address_id;
    }

    public String getPingjia() {
        return pingjia;
    }

    public String getCustomer_name() {
        return customer_name;
    }

    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }

    public void setPingjia(String pingjia) {
        this.pingjia = pingjia;
    }

    public String getPingjia_guanli() {
        return pingjia_guanli;
    }

    public void setPingjia_guanli(String pingjia_guanli) {
        this.pingjia_guanli = pingjia_guanli;
    }

    public void setAddress_id(Integer address_id) {
        this.address_id = address_id;
    }

    public void setTotalNumber(int totalNumber) {
        this.totalNumber = totalNumber;
    }
}