package com.byh.biyesheji.service;

import com.byh.biyesheji.pojo.Address;
import com.byh.biyesheji.pojo.Provider;

import java.util.List;

public interface ProviderService extends CrudService<Provider>{

    /**
     * 修改供应商信息
     */
    public void update(Provider provider);

}
