package com.byh.biyesheji.service;

import com.byh.biyesheji.pojo.Provider;
import com.byh.biyesheji.pojo.Purchase;

public interface PurchaseService extends CrudService<Purchase>{

    /**
     * 修改供应商信息
     */
    public void update(Purchase purchase);

}
