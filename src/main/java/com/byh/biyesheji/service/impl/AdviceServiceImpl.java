package com.byh.biyesheji.service.impl;

import com.byh.biyesheji.dao.AdviceMapper;
import com.byh.biyesheji.dao.ProviderMapper;
import com.byh.biyesheji.pojo.Advice;
import com.byh.biyesheji.pojo.Provider;
import com.byh.biyesheji.service.AdviceService;
import com.byh.biyesheji.service.ProviderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @ClassName AddressServiceImpl
 * @Description TODO
 * @Author 陈志星
 * @Date 2020/2/24 10:41
 */
@Service
public class AdviceServiceImpl implements AdviceService {

    @Autowired
    private AdviceMapper adviceMapper;
    @Override
    public List<Advice> list() {
        return adviceMapper.selectByExample(null);
    }

    @Override
    public void save(Advice entity) {
        adviceMapper.insert(entity);
    }

    @Override
    public void del(int id) {
        adviceMapper.deleteByPrimaryKey(id);
    }

    @Override
    public Advice get(int id) {
        return adviceMapper.selectByPrimaryKey(id);
    }
}
