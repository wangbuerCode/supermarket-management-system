package com.byh.biyesheji.service.impl;

import com.byh.biyesheji.dao.AddressMapper;
import com.byh.biyesheji.pojo.Address;
import com.byh.biyesheji.pojo.Customer;
import com.byh.biyesheji.service.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @ClassName AddressServiceImpl
 * @Description TODO
 * @Author 陈志星
 * @Date 2020/2/24 10:41
 */
@Service
public class AddressServiceImpl implements AddressService{
    @Autowired
    public AddressMapper addressMapper;

    @Override
    public void save(Address address) {
        addressMapper.insert(address);
    }

    @Override
    public Address get(int cstid) {
        return addressMapper.selectByPrimaryKey(cstid);
    }

    @Override
    public List<Address> list() {
        return addressMapper.selectByExample(null);
    }

    @Override
    public void del(int id) {
        addressMapper.deleteByPrimaryKey(id);
    }

    public void update(Address address){
        addressMapper.updateByPrimaryKey(address);
    }

    @Override
    public List<Address> getBycus_id(Integer id) {
        return addressMapper.getBycus_id(id);
    }


}
