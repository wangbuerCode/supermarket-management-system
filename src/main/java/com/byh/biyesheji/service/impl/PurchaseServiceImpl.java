package com.byh.biyesheji.service.impl;

import com.byh.biyesheji.dao.ProviderMapper;
import com.byh.biyesheji.dao.PurchaseMapper;
import com.byh.biyesheji.pojo.Provider;
import com.byh.biyesheji.pojo.Purchase;
import com.byh.biyesheji.service.ProviderService;
import com.byh.biyesheji.service.PurchaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @ClassName AddressServiceImpl
 * @Description TODO
 * @Author 陈志星
 * @Date 2020/2/24 10:41
 */
@Service
public class PurchaseServiceImpl implements PurchaseService {

    @Autowired
    private PurchaseMapper purchaseMapper;
    @Override
    public void update(Purchase purchase) {
        purchaseMapper.updateByPrimaryKey(purchase);
    }

    @Override
    public List<Purchase> list() {
        return purchaseMapper.selectByExample(null);
    }

    @Override
    public void save(Purchase entity) {
        purchaseMapper.insert(entity);
    }

    @Override
    public void del(int id) {
        purchaseMapper.deleteByPrimaryKey(id);
    }

    @Override
    public Purchase get(int id) {
        return purchaseMapper.selectByPrimaryKey(id);
    }
}
