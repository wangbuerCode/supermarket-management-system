package com.byh.biyesheji.service.impl;

import com.byh.biyesheji.dao.AddressMapper;
import com.byh.biyesheji.dao.ProviderMapper;
import com.byh.biyesheji.pojo.Address;
import com.byh.biyesheji.pojo.Provider;
import com.byh.biyesheji.service.AddressService;
import com.byh.biyesheji.service.ProviderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @ClassName AddressServiceImpl
 * @Description TODO
 * @Author 陈志星
 * @Date 2020/2/24 10:41
 */
@Service
public class ProviderServiceImpl implements ProviderService {


    @Autowired
    private ProviderMapper providerMapper;
    @Override
    public void update(Provider provider) {
        providerMapper.updateByPrimaryKey(provider);
    }

    @Override
    public List<Provider> list() {
        return providerMapper.selectByExample(null);
    }

    @Override
    public void save(Provider entity) {
        providerMapper.insert(entity);
    }

    @Override
    public void del(int id) {
        providerMapper.deleteByPrimaryKey(id);
    }

    @Override
    public Provider get(int id) {
        return providerMapper.selectByPrimaryKey(id);
    }
}
