package com.byh.biyesheji.service.impl;

import com.byh.biyesheji.dao.PurchaseMapper;
import com.byh.biyesheji.dao.WasteMapper;
import com.byh.biyesheji.pojo.Purchase;
import com.byh.biyesheji.pojo.Waste;
import com.byh.biyesheji.service.PurchaseService;
import com.byh.biyesheji.service.WasteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @ClassName AddressServiceImpl
 * @Description TODO
 * @Author 陈志星
 * @Date 2020/2/24 10:41
 */
@Service
public class WasteServiceImpl implements WasteService {

    @Autowired
    private WasteMapper wasteMapper;

    @Override
    public List<Waste> list() {
        return wasteMapper.selectByExample(null);
    }

    @Override
    public void save(Waste entity) {
        wasteMapper.insert(entity);
    }

    @Override
    public void del(int id) {
        wasteMapper.deleteByPrimaryKey(id);
    }

    @Override
    public Waste get(int id) {
        return wasteMapper.selectByPrimaryKey(id);
    }

    @Override
    public void update(Waste waste) {
        wasteMapper.updateByPrimaryKey(waste);
    }
}
