package com.byh.biyesheji.service;

import com.byh.biyesheji.pojo.Purchase;
import com.byh.biyesheji.pojo.Waste;

public interface WasteService extends CrudService<Waste>{

    /**
     * 修改供应商信息
     */
    public void update(Waste waste);

}
