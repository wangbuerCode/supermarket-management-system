package com.byh.biyesheji.service;

import com.byh.biyesheji.pojo.Address;

import java.util.List;

public interface AddressService extends CrudService<Address>{

    /**
     * 修改地址信息
     */
    public void update(Address address);
    /**
     * 根据当前登录人获取登录人常用地址
     *
     */
    public List<Address> getBycus_id(Integer id);

}
