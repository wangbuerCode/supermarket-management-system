package com.byh.biyesheji.dao;

import com.byh.biyesheji.pojo.Address;
import com.byh.biyesheji.pojo.AddressExample;
import com.byh.biyesheji.pojo.Provider;
import com.byh.biyesheji.pojo.ProviderExample;

import java.util.List;

public interface ProviderMapper extends CrudDao<Provider> {

    List<Provider> selectByExample(ProviderExample example);
}
