package com.byh.biyesheji.dao;

import com.byh.biyesheji.pojo.Address;
import com.byh.biyesheji.pojo.AddressExample;
import com.byh.biyesheji.pojo.Customer;
import com.byh.biyesheji.pojo.CustomerExample;

import java.util.List;

public interface AddressMapper extends CrudDao<Address>{

    List<Address> selectByExample(AddressExample example);

    List<Address> getBycus_id(Integer customer_id);

}