package com.byh.biyesheji.dao;

import com.byh.biyesheji.pojo.Purchase;
import com.byh.biyesheji.pojo.PurchaseExample;
import com.byh.biyesheji.pojo.Waste;
import com.byh.biyesheji.pojo.WasteExample;

import java.util.List;

public interface WasteMapper extends CrudDao<Waste> {

    List<Waste> selectByExample(WasteExample example);
}
