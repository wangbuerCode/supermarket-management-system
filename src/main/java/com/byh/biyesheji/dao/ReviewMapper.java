package com.byh.biyesheji.dao;

import com.byh.biyesheji.pojo.Review;
import com.byh.biyesheji.pojo.ReviewExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ReviewMapper extends CrudDao<Review>{

    List<Review> selectByExample(ReviewExample example);

    List<Review> getReview(@Param("pid") int pid, @Param("cid") int cid);

}