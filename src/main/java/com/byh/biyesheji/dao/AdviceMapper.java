package com.byh.biyesheji.dao;

import com.byh.biyesheji.pojo.*;

import java.util.List;

public interface AdviceMapper extends CrudDao<Advice> {

    List<Advice> selectByExample(AdviceExample example);
}
