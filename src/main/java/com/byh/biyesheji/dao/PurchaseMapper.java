package com.byh.biyesheji.dao;

import com.byh.biyesheji.pojo.Provider;
import com.byh.biyesheji.pojo.ProviderExample;
import com.byh.biyesheji.pojo.Purchase;
import com.byh.biyesheji.pojo.PurchaseExample;

import java.util.List;

public interface PurchaseMapper extends CrudDao<Purchase> {

    List<Purchase> selectByExample(PurchaseExample example);
}
