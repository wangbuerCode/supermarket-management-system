<%--
  Created by IntelliJ IDEA.
  User: baiyuhong
  Date: 2018/11/19
  Time: 17:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ include file="../../foreinclude/foreHander.jsp"%>

<!--=============================================
=            breadcrumb area         =
=============================================-->

<div class="breadcrumb-area pt-15 pb-15">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <!--=======  breadcrumb container  =======-->

                <div class="breadcrumb-container">
                    <nav>
                        <ul>
                            <li class="parent-page"><a href="/fore/foreIndex">首页</a></li>
                            <li>注册</li>
                        </ul>
                    </nav>
                </div>

                <!--=======  End of breadcrumb container  =======-->
            </div>
        </div>
    </div>
</div>

<!--=====  End of breadcrumb area  ======-->

<!--=============================================
=            Login Register page content         =
=============================================-->

<div class="page-section mb-50">
    <div class="container">

        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-3 col-xs-12"></div>
            <div class="col-sm-12 col-md-12 col-lg-6 col-xs-12">
                <form action="/fore/foreRegister" method="post" class="loginForm" >

                    <div class="login-form">
                        <h4 class="login-title">Register</h4>

                        <div class="row">
                            <div class="col-md-6 col-12 mb-20">
                                <label>姓名</label>
                                <input class="mb-0" type="text" name="name" id="name" placeholder="Name">
                            </div>
                            <div class="col-md-6 col-12 mb-20">
                                <label>pwd</label>
                                <input class="mb-0" type="text" name="password" id="password" placeholder="">
                            </div>
                            <div class="col-md-12 mb-20">
                                <label>邮箱地址:</label>
                                <input class="mb-0" type="text" name="address" id="address" placeholder="Email Address">
                            </div>
                            <div class="col-md-6 mb-20">
                                <label>手机:</label>
                                <input class="mb-0" type="text"  name="phone" id="phone" placeholder="telphone">
                            </div>

                            <div class="col-12">
                                <button type="submit" class="register-button mt-0">注册</button>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>

<!--=====  End of Login Register page content  ======-->
<script src="${pageContext.request.contextPath}/js/jquery/2.0.0/jquery.min.js"></script>
<script type="text/javascript">
    $(function () {
        //验证不能为空
        $("form.loginForm").submit(function(){
            var tel= /^0{0,1}(13[0-9]|14[0-9]|15[0-9]|17[0-9]|18[0-9])[0-9]{8}$/;//手机号校验
            var mail= /^([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+@([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+\.[a-zA-Z]{2,3}$/;//邮箱校验
            var han = /^[\u4e00-\u9fa5]+$/;         //中文校验
            var flag = true;
            if(0==$("#name").val().length){
                alert("账号不能为空");
                flag = false;
            }else if(han.test($("#name").val())){
                alert("账号中不能有中文");
                flag = false;
            }
            if($("#password").val().length < 6 || $("#password").val().length > 10){
                alert("密码长度需在6-10位之间");
                flag = false;
            }
            if($("#address").val().length == 0){
                alert("邮箱不能为空");
                return false;
            }else if(!mail.test($("#address").val())){
                alert("邮箱格式错误");
                flag = false;
            }
            if($("#phone").val().length == 0){
                alert("手机号不能为空");
                flag = false;
            }else if(!tel.test($("#phone").val())){
                alert("手机号格式错误");
                flag = false;
            }
            return flag;
        });
    })
</script>

<%@ include file="../../foreinclude/foreFooter.jsp"%>
