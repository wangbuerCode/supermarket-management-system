<%--
  Created by IntelliJ IDEA.
  User: baiyuhong
  Date: 2018/11/26
  Time: 11:11
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ include file="../../foreinclude/foreHander1.jsp"%>

<!--=============================================
=            breadcrumb area         =
=============================================-->

<div class="breadcrumb-area pt-15 pb-15">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <!--=======  breadcrumb container  =======-->

                <div class="breadcrumb-container">
                    <nav>
                        <ul>
                            <li class="parent-page"><a href="/fore/foreIndex">首页</a></li>
                            <li>留言信息</li>
                        </ul>
                    </nav>
                </div>

                <!--=======  End of breadcrumb container  =======-->
            </div>
        </div>
    </div>
</div>

<!--=====  End of breadcrumb area  ======-->

<!--=============================================
=            blog page content         =
=============================================-->

<div class="blog-page-content mb-50">
    <div class="container">
        <div class="row">


            <div class="col-lg-12 order-1">
                <!--=======  blog post container  =======-->

                <div class="blog-single-post-container mb-30">

                    <!--=======  post title  =======-->


                    <h3 class="post-title">常用地址</h3>

                    <!--=======  End of post title  =======-->

                    <div class="post-content mb-40">


                        <blockquote>
                            <p>
                                用户可添加自己的常用地址
                                <button style="float:right" class="btn btn-default"><a href="javascript:;" onclick="fabuzixun();">增加</a></button>
                            </p>

                        </blockquote>

                    </div>
                    <!--=======  End of Post content  =======-->
                </div>

                <!--=======  End of blog post container  =======-->

                <!--=============================================
                =            Comment section         =
                =============================================-->

                <div class="comment-section mb-md-30 mb-sm-30">


                    <!--=======  comment container  =======-->

                    <div class="comment-container mb-40">
                        <!--=======  single comment  =======-->
                        <c:forEach items="${list}" var="z">
                        <div class="single-comment">
                            <!--
                                <span class="reply-btn"><a href="#">Reply</a></span>
                            -->
                            <div class="image">
                                <img src="assets/images/blog-image/comment-icon.png" alt="">
                            </div>
                            <div class="content">
                                收货人姓名 ： <h3 class="user">${z.gen_name}</span></h3><span style="float: right;padding-right: 40px;"><a href="#" onclick="deladd(${z.id})">删除</a></span>
                                收货人电话 ： <p class="comment-text">${z.telphone}</p>
                                收获人地址 ： <p class="comment-text">${z.gen_address}</p>
                            </div>
                        </div>
                        </c:forEach>

                        <!--=======  End of single comment  =======-->
                    </div>
                    <!--=======  End of comment container  =======-->
                 <!--=======  End of comment form container  =======-->
                </div>
                <!--=====  End of Comment section  ======-->
            </div>
        </div>
    </div>
</div>

<!--=====  End of blog page content  ======-->
<!-- 模态框（Modal） -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog"  id="myModal" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!--登陆框头部-->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    ×
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    添加地址
                </h4>
            </div>
            <!--登陆框中间部分(from表单)-->
            <div class="modal-body">
                <!--评价-->
                <div class="form-group">
                    <label class="col-sm-4 control-label">输入信息</label>
                    <div class="col-sm-12">
                        收货人姓名：<input class="form-control" name="gen_name" id="gen_name" placeholder="" required="required"></input>
                        收获人电话：<input class="form-control" name="telphone" id="telphone" placeholder="" required="required"></input>
                        收货人地址：<input class="form-control" name="gen_address" id="gen_address" placeholder="" required="required"></input>
                    </div>
                </div>
                <input type="hidden" name="customer_id" value="${cst.id}"/>
                <!--登陆按钮-->
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                        <button type="button" class="btn btn-default"  id="modalFabu">确定</button>
                    </div>
                </div>
                <%--</form>--%>
            </div>
        </div>
    </div>
</div>

<script src="${pageContext.request.contextPath}/js/jquery/2.0.0/jquery.min.js"></script>
<script>
    function fabuzixun(){
        $('#myModal').modal('show');
    }
    function deladd(add_id){
        $.ajax({
                url:"address_del",
                type:"get",
                contentType:"application/x-www-form-urlencoded; charset=utf-8",
                data:{"add_id":add_id},
                success: function (result) {
                    if(result=="success"){
                        alert("删除成功");
                        location.reload();
                    }
                }
            });
    }
    $(function () {
        $("#modalFabu").click(function () {
            var gen_name = $("#gen_name").val();
            var telphone = $("#telphone").val();
            var gen_address = $("#gen_address").val();
            var tel= /^0{0,1}(13[0-9]|14[0-9]|15[0-9]|17[0-9]|18[0-9])[0-9]{8}$/;//手机号校验
            var mail= /^([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+@([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+\.[a-zA-Z]{2,3}$/;//邮箱校验
            var han = /^[\u4e00-\u9fa5]+$/;         //中文校验
            var flag = true;
            if(gen_name.length == 0){
                alert("收款人不能为空");
                flag = false;
            }
            if(gen_address.length == 0){
                alert("收获不能为空");
                return false;
            }
            if(tel.length == 0){
                alert("手机号不能为空");
                flag = false;
            }else if(!tel.test(telphone)){
                alert("手机号格式错误");
                flag = false;
            }
            if(flag){
                $.ajax({
                    url:"address_add",
                    type:"get",
                    contentType:"application/x-www-form-urlencoded; charset=utf-8",
                    data:{"gen_name":gen_name,"telphone":telphone,"gen_name":gen_name,"gen_address":gen_address},
                    success: function (result) {
                        if(result=="success"){
                            alert("添加成功");
                            $('#myModal').modal('hide');
                            location.reload();
                        }
                    }
                });
            }
//            $.get(
//                "foreZixunadd",
//
//                {"content":content},
//                function (result) {
//                    if(result=="success"){
//                        alert("已提交，请等待管理员审核！");
//                        $('#myModal').modal('hide');
//                    }
//                }
//            );
            //get结束
        });
    })
</script>
<!--====  End of My Account page content  ====-->
<%@ include file="../../foreinclude/foreFooter.jsp"%>