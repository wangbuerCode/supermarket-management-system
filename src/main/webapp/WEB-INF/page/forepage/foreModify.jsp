<%--
  Created by IntelliJ IDEA.
  User: baiyuhong
  Date: 2018/11/19
  Time: 17:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ include file="../../foreinclude/foreHander.jsp"%>

<!--=============================================
=            breadcrumb area         =
=============================================-->

<div class="breadcrumb-area pt-15 pb-15">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <!--=======  breadcrumb container  =======-->

                <div class="breadcrumb-container">
                    <nav>
                        <ul>
                            <li class="parent-page"><a href="/fore/foreIndex">首页</a></li>
                            <li>修改个人信息</li>
                        </ul>
                    </nav>
                </div>

                <!--=======  End of breadcrumb container  =======-->
            </div>
        </div>
    </div>
</div>

<!--=====  End of breadcrumb area  ======-->

<!--=============================================
=            Login Register page content         =
=============================================-->

<div class="page-section mb-50">
    <div class="container">

        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-3 col-xs-12"></div>
            <div class="col-sm-12 col-md-12 col-lg-6 col-xs-12">
                <form action="/fore/foreMod" method="post" class="loginForm" >

                    <div class="login-form">
                        <h4 class="login-title">修改</h4>
                        <input type="hidden" value="${cst.id}" name="id">
                        <div class="row">
                            <div class="col-md-6 col-12 mb-20">
                                <label>账号</label>
                                <input class="mb-0" type="text" value="${cst.name}" name="name" id="name" placeholder="账号">
                            </div>
                            <div class="col-md-6 col-12 mb-20">
                                <label>旧密码</label>
                                <input class="mb-0" type="password" value="${cst.password}" name="password" id="password1" placeholder="">
                            </div>
                            <div class="col-md-6 col-12 mb-20">
                                <label>新密码</label>
                                <input class="mb-0" type="password" id="password2" placeholder="">
                            </div>
                            <div class="col-md-6 col-12 mb-20">
                                <label>确认密码</label>
                                <input class="mb-0" type="password" id="password3" placeholder="">
                            </div>
                            <div class="col-md-12 mb-20">
                                <label>邮箱地址:</label>
                                <input class="mb-0" type="text" value="" name="address" id="address" placeholder="邮箱地址">
                            </div>
                            <div class="col-md-6 mb-20">
                                <label>手机:</label>
                                <input class="mb-0" type="text" value="${cst.phone}"  name="phone" id="phone" placeholder="手机号">
                            </div>

                            <div class="col-12">
                                <button type="submit" class="register-button mt-0">修改</button>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>

<!--=====  End of Login Register page content  ======-->
<script src="${pageContext.request.contextPath}/js/jquery/2.0.0/jquery.min.js"></script>
<script type="text/javascript">
    $(function () {
        //验证不能为空
        $("form.loginForm").submit(function(){
            debugger
            var tel= /^0{0,1}(13[0-9]|14[0-9]|15[0-9]|17[0-9]|18[0-9])[0-9]{8}$/;//手机号校验
            var mail= /^([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+@([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+\.[a-zA-Z]{2,3}$/;//邮箱校验
            var han = /^[\u4e00-\u9fa5]+$/;         //中文校验
            var flag = true;
            var password1 = $("#password1").val();//旧密码
            var password2 = $("#password2").val();//新密码
            var password3 = $("#password3").val();//确认密码
            if(password2 != null || password3 != null){
                $("#password1").removeAttr("name");
                $("#password2").attr("name","password");
            }
            if(password2 != password3){
                alert("俩次密码输入不一致");
                flag = false;
            }
            if(0==$("#name").val().length){
                alert("账号不能为空");
                flag = false;
            }else if(han.test($("#name").val())){
                alert("账号中不能有中文");
                flag = false;
            }
            if($("input[name='password']").val().length < 6 || $("input[name='password']").val().length > 10){
                alert("密码长度需在6-10位之间");
                flag = false;
            }
            if($("#address").val().length == 0){
                alert("邮箱不能为空");
                return false;
            }else if(!mail.test($("#address").val())){
                alert("邮箱格式错误");
                flag = false;
            }
            if($("#phone").val().length == 0){
                alert("手机号不能为空");
                flag = false;
            }else if(!tel.test($("#phone").val())){
                alert("手机号格式错误");
                flag = false;
            }
            return flag;
        });
    })
    $("#password1").change(function () {
        $("#password1").removeAttr("name");
        $("#password2").attr("name","password");
    });

</script>

<%@ include file="../../foreinclude/foreFooter.jsp"%>
