<%--
  Created by IntelliJ IDEA.
  User: baiyuhong
  Date: 2018/11/26
  Time: 11:11
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ include file="../../foreinclude/foreHander1.jsp"%>

<!--=============================================
=            breadcrumb area         =
=============================================-->

<div class="breadcrumb-area pt-15 pb-15">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <!--=======  breadcrumb container  =======-->

                <div class="breadcrumb-container">
                    <nav>
                        <ul>
                            <li class="parent-page"><a href="/fore/foreIndex">首页</a></li>
                            <li>资讯信息</li>
                        </ul>
                    </nav>
                </div>

                <!--=======  End of breadcrumb container  =======-->
            </div>
        </div>
    </div>
</div>

<!--=====  End of breadcrumb area  ======-->

<!--=============================================
=            blog page content         =
=============================================-->

<div class="blog-page-content mb-50">
    <div class="container">
        <div class="row">


            <div class="col-lg-12 order-1">
                <!--=======  blog post container  =======-->

                <div class="blog-single-post-container mb-30">

                    <!--=======  post title  =======-->


                    <h3 class="post-title">资讯</h3>

                    <!--=======  End of post title  =======-->

<%--                    <div class="post-content mb-40">--%>


<%--                        <blockquote>--%>
<%--                            <p>--%>
<%--                                用户可发布自己的问题，管理员审核通过后进行显示--%>
<%--                                <button style="float:right" class="btn btn-default"><a href="javascript:;" onclick="fabuzixun();">发布</a></button>--%>
<%--                            </p>--%>

<%--                        </blockquote>--%>

<%--                    </div>--%>
                    <!--=======  End of Post content  =======-->
                </div>

                <!--=======  End of blog post container  =======-->

                <!--=============================================
                =            Comment section         =
                =============================================-->

                <div class="comment-section mb-md-30 mb-sm-30">


                    <!--=======  comment container  =======-->

                    <div class="comment-container mb-40">
                        <!--=======  single comment  =======-->
                        <c:forEach items="${list}" var="z">
                        <div class="single-comment">
                            <!--
                                <span class="reply-btn"><a href="#">Reply</a></span>
                            -->
                            <div class="image">
                                <img src="assets/images/blog-image/comment-icon.png" alt="">
                            </div>
                            <div class="content">
                                <p class="comment-text">${z.content}.</p>
                                <p class="comment-text">${z.addtime}.</p>
                            </div>
                        </div>
                        </c:forEach>

                        <!--=======  End of single comment  =======-->
                    </div>
                    <!--=======  End of comment container  =======-->
                 <!--=======  End of comment form container  =======-->
                </div>
                <!--=====  End of Comment section  ======-->
            </div>
        </div>
    </div>
</div>
<script src="${pageContext.request.contextPath}/js/jquery/2.0.0/jquery.min.js"></script>
<!--====  End of My Account page content  ====-->
<%@ include file="../../foreinclude/foreFooter.jsp"%>