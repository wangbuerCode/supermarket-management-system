<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: baiyuhong
  Date: 2018/10/4
  Time: 21:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@include file="../../include/publicMeta.jsp"%>
<%@include file="../../include/publicHeader.jsp"%>
<%@include file="../../include/publicMenu.jsp"%>

<section class="Hui-article-box">
    <nav class="breadcrumb"><i class="Hui-iconfont">&#xe67f;</i> 首页
        <span class="c-gray en">&gt;</span>
        资讯管理
        <span class="c-gray en">&gt;</span>
        资讯列表 <a class="btn btn-success radius r" style="line-height:1.6em;margin-top:3px" href="javascript:location.replace(location.href);" title="刷新" ><i class="Hui-iconfont">&#xe68f;</i></a> </nav>
    <div class="Hui-article">
        <article class="cl pd-20">
            <div class="cl pd-5 bg-1 bk-gray mt-20">
                <input type="text" class="input-text" style="width:150px" placeholder="输入资讯" id="content" name="">
                <button class="btn btn-secondary radius" onclick="admin_advice_add()">添加资讯</button>
                <span class="r">共有数据：<strong>${totals}</strong> 条</span>
            </div>
            <table class="table table-border table-bordered table-bg" id="mytable">
                <thead>
                <tr>
                    <th scope="col" colspan="10">资讯列表</th>
                </tr>
                <tr class="text-c">
                    <th>资讯内容</th>
                    <th>发布时间</th>
                    <th width="150">操作</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${list}" var="z">
                <tr class="text-c">
                    <td>${z.content}</td>
                    <td>${z.addtime}</td>
                    <td>
                        <a deleteLink="true" title="删除" href="/advice/del?id=${z.id}"
                           class="ml-5" style="text-decoration:none">
                            <span class="label label-success radius">删除</span>
                        </a>
                    </td>
                </tr>
                </c:forEach>
                </tbody>
            </table>
        </article>
        <article class="cl pd-20">
            <%@include file="../adminPage.jsp"%>
        </article>
    </div>
</section>
<%@include file="../../include/publicFooter.jsp"%>

<script type="text/javascript">

    $(function(){
        $("a").click(function(){
            var deleteLink = $(this).attr("deleteLink");
            console.log(deleteLink);
            if("true"==deleteLink){
                var confirmDelete = confirm("确认要删除");
                if(confirmDelete)
                    return true;
                return false;

            }
        });
    })
    function admin_advice_add(){
        var content = $("#content").val();
        $.get(
            "addAdvice",
            {"content":content},
            function (result) {
                location.reload();
            }
        );
    }
</script>

</body>
</html>
