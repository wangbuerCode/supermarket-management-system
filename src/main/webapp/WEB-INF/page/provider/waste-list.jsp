<%--
  Created by IntelliJ IDEA.
  User: baiyuhong
  Date: 2018/10/11
  Time: 17:31
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@include file="../../include/publicMeta.jsp"%>
<%@include file="../../include/publicHeader.jsp"%>
<%@include file="../../include/publicMenu.jsp"%>

<section class="Hui-article-box">
    <nav class="breadcrumb"><i class="Hui-iconfont">&#xe67f;</i> 首页
        <span class="c-gray en">&gt;</span>
        浪费信息管理
        <span class="c-gray en">&gt;</span>
        浪费信息列表 <a class="btn btn-success radius r" style="line-height:1.6em;margin-top:3px" href="javascript:location.replace(location.href);" title="刷新" ><i class="Hui-iconfont">&#xe68f;</i></a> </nav>
    <div class="Hui-article">
        <article class="cl pd-20">
            <div class="cl pd-5 bg-1 bk-gray mt-20">
                <span class="l">
                    <a href="javascript:;" onclick="admin_add('添加采购信息','wasteAddUI','800','500')" class="btn btn-primary radius">
                        <i class="Hui-iconfont">&#xe600;</i> 添加浪费信息
                    </a>
                </span>
                <span class="r">共有数据：<strong>${totals}</strong> 条</span>
            </div>
            <table class="table table-border table-bordered table-bg" id="mytable">
                <thead>
                <tr>
                    <th scope="col" colspan="12">浪费信息列表</th>
                </tr>
                <tr class="text-c">
                    <th width="25"><input type="checkbox" name="" value=""></th>
                    <th width="">编号</th>
                    <th width="">物品名</th>
                    <th width="">单价</th>
                    <th width="">数量</th>
                    <th width="">总价</th>
                    <th width="">采购时间</th>
                    <th width="100">操作</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${list}" var="waste">
                    <tr class="text-c">
                        <td><input type="checkbox" value="1" name=""></td>
                        <td>${waste.id}</td>
                        <td>${waste.name}</td>
                        <td>${waste.price}</td>
                        <td>${waste.num}</td>
                        <td>${waste.tolp}</td>
                        <td>${waste.addtime}</td>
                        <td class="td-manage" >
<%--                            <a title="编辑" href="javascript:;" onclick="admin_edit('采购信息编辑','editPurchase?id=${purchase.id}','1','800','500')" class="ml-5" style="text-decoration:none">--%>
<%--                                <i class="Hui-iconfont">&#xe6df;</i>--%>
<%--                            </a>--%>
                            <a deleteLink="true" title="删除" href="/waste/del?id=${waste.id}"  class="ml-5" style="text-decoration:none">
                                <i class="Hui-iconfont">&#xe6e2;</i>
                            </a>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>

        </article>
        <article class="cl pd-20">
            <%@include file="../adminPage.jsp"%>
        </article>
    </div>
</section>


<%@include file="../../include/publicFooter.jsp"%>

<script type="text/javascript">
    $(function(){
        $("a").click(function(){
            var deleteLink = $(this).attr("deleteLink");
            console.log(deleteLink);
            if("true"==deleteLink){
                var confirmDelete = confirm("确认要删除");
                if(confirmDelete)
                    return true;
                return false;

            }
        });
    })


    function admin_add(title,url,w,h){
        layer_show(title,url,w,h);
    }


    function admin_edit(title,url,id,w,h){
        layer_show(title,url,w,h);
    }



</script>

</body>
</html>
