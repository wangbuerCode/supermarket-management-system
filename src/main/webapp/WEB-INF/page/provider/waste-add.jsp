<%--
  Created by IntelliJ IDEA.
  User: baiyuhong
  Date: 2018/10/12
  Time: 10:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@include file="../../include/publicMeta.jsp"%>

<article class="cl pd-20">
    <form action="/waste/addWaste" method="post" enctype="multipart/form-data" class="form form-horizontal" id="form-admin-add"  target="_parent">
        <div class="row cl">
            <label class="form-label col-xs-4 col-sm-3">物品名称：</label>
            <div class="formControls col-xs-8 col-sm-9"> <span class="select-box" style="width:150px;">
<%--                <input type="hidden" name="providerid">--%>
				<select class="select" name="proid" size="1" id="proid">
                    <option></option>
                    <c:forEach items="${purchaselist}" var="provider">
                        <option value="${provider.id}">${provider.name}</option>
                    </c:forEach>
				</select>
				</span>
            </div>
        </div>
        <div class="row cl">
            <label class="form-label col-xs-4 col-sm-3"><span class="c-red">*</span>物品单价：</label>
            <div class="formControls col-xs-8 col-sm-9">
                <input type="text" class="input-text" readonly autocomplete="off" value="" placeholder="" id="price" name="price">
            </div>
        </div>
        <div class="row cl">
            <label class="form-label col-xs-4 col-sm-3"><span class="c-red">*</span>数量：</label>
            <div class="formControls col-xs-8 col-sm-9">
                <input type="text" class="input-text" autocomplete="off" value="" placeholder="" id="num" name="num">
            </div>
        </div>
        <div class="row cl">
            <label class="form-label col-xs-4 col-sm-3"><span class="c-red">*</span>总价：</label>
            <div class="formControls col-xs-8 col-sm-9">
                <input type="text" class="input-text" autocomplete="off" value="" placeholder="" id="tolp" name="tolp" readonly>
            </div>
        </div>
        <div class="row cl">
            <div class="col-xs-8 col-sm-9 col-xs-offset-4 col-sm-offset-3">
                <input class="btn btn-primary radius" type="submit" value="&nbsp;&nbsp;提交&nbsp;&nbsp;">
            </div>
        </div>
    </form>
</article>
<%@include file="../../include/publicFooter.jsp"%>

<script type="text/javascript">
    $(function(){
        $('.skin-minimal input').iCheck({
            checkboxClass: 'icheckbox-blue',
            radioClass: 'iradio-blue',
            increaseArea: '20%'
        });
    });
    $("#proid").change(function () {
        var proid = $(this).val();
        $.get(
            "getByProid",
            {"id":proid},
            function (result) {
                $("#price").val(result.price);
            }
        );
    });
    $("#num").blur(function (data) {
        var price = $("#price").val();
        var num = $(this).val();
        $("#tolp").val(price * num);
    });
</script>
<!--/请在上方写此页面业务相关的脚本-->
</body>
</html>