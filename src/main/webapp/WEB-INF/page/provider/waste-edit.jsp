<%--
  Created by IntelliJ IDEA.
  User: baiyuhong
  Date: 2018/10/12
  Time: 10:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@include file="../../include/publicMeta.jsp"%>

<article class="cl pd-20">
    <form action="/purchase/editPurchase" method="post" enctype="multipart/form-data" class="form form-horizontal" id="form-admin-add"  target="_parent">
        <div class="row cl">
            <label class="form-label col-xs-4 col-sm-3"><span class="c-red">*</span>商品名称：</label>
            <div class="formControls col-xs-8 col-sm-9">
                <input type="text" class="input-text" value="${purchase.name}" placeholder="" id="name" name="name">
            </div>
        </div>
        <div class="row cl">
            <label class="form-label col-xs-4 col-sm-3"><span class="c-red">*</span>商品单价：</label>
            <div class="formControls col-xs-8 col-sm-9">
                <input type="text" class="input-text" autocomplete="off" value="${purchase.price}" placeholder="" id="price" name="price">
            </div>
        </div>
        <div class="row cl">
            <label class="form-label col-xs-4 col-sm-3"><span class="c-red">*</span>数量：</label>
            <div class="formControls col-xs-8 col-sm-9">
                <input type="text" class="input-text" autocomplete="off" value="${purchase.num}" placeholder="" id="num" name="num">
            </div>
        </div>

        <div class="row cl">
            <label class="form-label col-xs-4 col-sm-3">供应商：</label>
            <div class="formControls col-xs-8 col-sm-9">
                <input type="hidden" name="providerid" value="${purchase.providerid}">
                <span class="select-box" style="width:150px;">
				<select class="select" name="providername" size="1">
                    <c:forEach items="${providerlist}" var="provider">
                        <c:set var="crrent" value="false" />
                        <c:if test="${crrentProvider.company==provider.company}">
                            <c:set var="crrent" value="true" />
                        </c:if>
                        <option value="${provider.name}" ${crrent?"selected='selected'":"" } >${provider.name}</option>
                    </c:forEach>
				</select>
				</span>
            </div>
        </div>
            <input type="hidden" name="id" value="${purchase.id}" />
        <div class="row cl">
            <div class="col-xs-8 col-sm-9 col-xs-offset-4 col-sm-offset-3">
                <input class="btn btn-primary radius" type="submit" value="&nbsp;&nbsp;提交&nbsp;&nbsp;">
            </div>
        </div>
    </form>
</article>
<%@include file="../../include/publicFooter.jsp"%>

<script type="text/javascript">
    $(function(){
        $('.skin-minimal input').iCheck({
            checkboxClass: 'icheckbox-blue',
            radioClass: 'iradio-blue',
            increaseArea: '20%'
        });


    });
</script>
<!--/请在上方写此页面业务相关的脚本-->
</body>
</html>